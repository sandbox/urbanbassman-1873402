; Core version
; ------------
 
core = 7.x
  
; API version
; ------------
  
api = 2
  
; Modules
; --------
projects[admin_menu][version] = 3.0-rc3
projects[admin_menu][type] = "module"
projects[badbehavior][version] = 2.2213
projects[badbehavior][type] = "module"
projects[backup_migrate][version] = 2.4
projects[backup_migrate][type] = "module"
projects[module_filter][version] = 1.7
projects[module_filter][type] = "module"
;projects[contemplate][version] = 1.0-rc3
;projects[contemplate][type] = "module"
;projects[ctools][version] = 1.2
projects[ctools][version] = 1.2+7-dev
projects[ctools][type] = "module"
projects[context][version] = 3.0-beta6
projects[context][type] = "module"
projects[calendar][version] = 3.4
projects[calendar][type] = "module"
projects[countries][version] = 2.1
projects[countries][type] = "module"
projects[date][version] = 2.6
projects[date][type] = "module"
projects[date_ical][version] = 1.1
projects[date_ical][type] = "module"
projects[devel][version] = 1.3
projects[devel][type] = "module"
projects[features][version] = 1.0
projects[features][type] = "module"
;projects[content_taxonomy][version] = 1.0-beta1
;projects[content_taxonomy][type] = "module"
projects[email][version] = 1.2
projects[email][type] = "module"
projects[field_group][version] = 1.1
projects[field_group][type] = "module"
;projects[filefield_paths][version] = 1.0-beta3
;projects[filefield_paths][type] = "module"
projects[link][version] = 1.0
projects[link][type] = "module"
projects[flag][version] = 2.0
projects[flag][type] = "module"
;projects[i18n][version] = 1.7
;projects[i18n][type] = "module"
;projects[og][version] = 2.0-beta4
projects[og][version] = 2.0-beta4+2-dev
projects[og][type] = "module"
projects[og_extras][version] = 1.1
projects[og_extras][type] = "module"
projects[og_webform][version] = 1.0-beta1
projects[og_webform][type] = "module"
projects[advanced_forum][version] = 2.0
projects[advanced_forum][type] = "module"
projects[advanced_help][version] = 1.0
projects[advanced_help][type] = "module"
projects[better_formats][version] = 1.0-beta1
projects[better_formats][type] = "module"
projects[entity][version] = 1.0-rc3
projects[entity][type] = "module"
projects[entityreference][version] = 1.0
projects[entityreference][type] = "module"
projects[feeds][version] = 2.0-alpha7
projects[feeds][type] = "module"
projects[gmap][version] = 1.0-beta1
projects[gmap][type] = "module"
projects[google_analytics][version] = 1.3
projects[google_analytics][type] = "module"
projects[invisimail][version] = 1.1
projects[invisimail][type] = "module"
projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][type] = "module"
projects[libraries][version] = 2.0
projects[libraries][type] = "module"
projects[location][version] = 3.0-alpha1
projects[location][type] = "module"
projects[media][version] = 1.2
projects[media][type] = "module"
projects[media_gallery][version] = 1.0-beta8
projects[media_gallery][type] = "module"
projects[media_flickr][version] = 1.0-alpha3
projects[media_flickr][type] = "module"
projects[media_youtube][version] = 2.0-rc1
projects[media_youtube][type] = "module"
projects[menu_import][version] = 1.3
projects[menu_import][type] = "module"
projects[multiform][version] = 1.0
projects[multiform][type] = "module"
projects[migrate][version] = 2.5
projects[migrate][type] = "module"
projects[mollom][version] = 2.3
projects[mollom][type] = "module"
projects[oauth][version] = 3.0
projects[oauth][type] = "module"
projects[node_export][version] = 3.0
projects[node_export][type] = "module"
projects[nodeownership][version] = 1.3
projects[nodeownership][type] = "module"
projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[panelizer][version] = 2.0
projects[panelizer][type] = "module"
projects[rate][version] = 1.6
projects[rate][type] = "module"
projects[scheduler][version] = 1.0
projects[scheduler][type] = "module"
projects[session_api][version] = 1.0-rc1
projects[session_api][type] = "module"
projects[talk][version] = 1.0
projects[talk][type] = "module"
projects[token][version] = 1.4
projects[token][type] = "module"
projects[twitter][version] = 5.1
projects[twitter][type] = "module
projects[panels][version] = 3.3
projects[panels][type] = "module"
projects[print][version] = 1.2
projects[print][type] = "module"
projects[rules][version] = 2.2
projects[rules][type] = "module"
projects[page_title][version] = 2.7
projects[page_title][type] = "module"
projects[profile2][version] = 1.2
projects[profile2][type] = "module"
projects[realname][version] = 1.0
projects[realname][type] = "module"
projects[strongarm][version] = 2.0-rc1
projects[strongarm][type] = "module"
projects[uuid][version] = 1.0-alpha3
projects[uuid][type] = "module"
projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"
projects[views][version] = 3.5
projects[views][type] = "module"
projects[views_bulk_operations][version] = 3.1
projects[views_bulk_operations][type] = "module"
;projects[votingapi][version] = 2.10
;projects[votingapi][type] = "module"
projects[webform][version] = 3.18
projects[webform][type] = "module"
projects[wordpress_migrate][version] = 2.2
projects[wordpress_migrate][type] = "module"

; CoderDojo Features Packages
; ---------------------------
projects[coderdojo_content_type_dojo][type] = "module"
projects[coderdojo_content_type_dojo][download][type] = "git"
projects[coderdojo_content_type_dojo][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1874990.git"
projects[coderdojo_content_type_dojo][subdir] = "coderdojo"

projects[coderdojo_content_type_wiki_page][type] = "module"
projects[coderdojo_content_type_wiki_page][download][type] = "git"
projects[coderdojo_content_type_wiki_page][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875308.git"
projects[coderdojo_content_type_wiki_page][subdir] = "coderdojo"

projects[coderdojo_views_dojo_list][type] = "module"
projects[coderdojo_views_dojo_list][download][type] = "git"
projects[coderdojo_views_dojo_list][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875536.git"
projects[coderdojo_views_dojo_list][subdir] = "coderdojo"

;projects[coderdojo_fields_node_dojo][type] = "module"
;projects[coderdojo_fields_node_dojo][download][type] = "git"
;projects[coderdojo_fields_node_dojo][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1874996.git"
;projects[coderdojo_fields_node_dojo][subdir] = "coderdojo"

projects[coderdojo_fields_node_media_gallery][type] = "module"
projects[coderdojo_fields_node_media_gallery][download][type] = "git"
projects[coderdojo_fields_node_media_gallery][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875000.git"
projects[coderdojo_fields_node_media_gallery][subdir] = "coderdojo"

;projects[coderdojo_taxonomies][type] = "module"
;projects[coderdojo_taxonomies][download][type] = "git"
;projects[coderdojo_taxonomies][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875012.git"
;projects[coderdojo_taxonomies][subdir] = "coderdojo"

projects[coderdojo_wysiwyg_profile][type] = "module"
projects[coderdojo_wysiwyg_profile][download][type] = "git"
projects[coderdojo_wysiwyg_profile][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875008.git"
projects[coderdojo_wysiwyg_profile][subdir] = "coderdojo"

projects[coderdojo_menu_links_main_menu][type] = "module"
projects[coderdojo_menu_links_main_menu][download][type] = "git"
projects[coderdojo_menu_links_main_menu][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875550.git"
projects[coderdojo_menu_links_main_menu][subdir] = "coderdojo"

projects[coderdojo_node_export_wiki_pages][type] = "module"
projects[coderdojo_node_export_wiki_pages][download][type] = "git"
projects[coderdojo_node_export_wiki_pages][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1875834.git"
projects[coderdojo_node_export_wiki_pages][subdir] = "coderdojo"

projects[coderdojo_text_format][type] = "module"
projects[coderdojo_text_format][download][type] = "git"
projects[coderdojo_text_format][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1881642.git"
projects[coderdojo_text_format][subdir] = "coderdojo"



; Themes
; --------

projects[omega][type] = "module"
projects[omega][subdir] = "custom"

;omega supporting modules
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "custom"
projects[delta][type] = "module"
projects[delta][subdir] = "custom"

projects[coderdojo_theme][type] = "theme"
projects[coderdojo_theme][download][type] = "git"
projects[coderdojo_theme][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1880900.git"
projects[coderdojo_theme][subdir] = "custom"
  
; Libraries
; ---------
libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
libraries[jqueryui][download][type] = "file"
libraries[jqueryui][download][url] = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"


